section .text

%define new_line_sym 0xA
%define tab_sym 0x9
%define space_sym 0x20
%define null_term 0x00
%define read_code 0
%define write_code 1
%define exit_code 60

global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, exit_code
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax	
 .count:
	cmp byte[rdi+rax], null_term
	je .end
	inc rax
	jmp .count
 .end:
    ret
	
	
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov rsi, 1
	jmp print
	
print_err:
	mov rsi, 2
print:
	push rdi
	push rsi
	call string_length
	pop rdi
	pop rsi
	
	mov rdx, rax
	mov rax, write_code
	syscall
	
    ret

; Принимает код символа и выводит его в stdout
print_char:
	mov rax, write_code
	dec rsp
	mov [rsp], dil
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	inc rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, new_line_sym
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	xor rdx, rdx
	mov r10, rsp
	dec rsp
	mov byte[rsp], dl
 .loop:
    mov r11, 10
	div r11
	add rdx, '0'
	dec rsp
	mov byte[rsp], dl
	xor rdx, rdx
	test rax, rax
	jnz .loop 
	mov rdi, rsp
	
	push r10
	call print_string
	pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
	cmp rdi, 0
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
 .print:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	xor r10, r10
	xor r11, r11
 .loop:
	mov r10b, [rdi]
	mov r11b, [rsi]
	cmp r10b, r11b
	jne .fail
	inc rdi
	inc rsi
	test r10b, r10b
	jnz .loop
	mov rax, 1
	jmp .end
 .fail:
	xor rax, rax
 .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
	xor rdi, rdi
	dec rsp
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	jz .end
	mov al, [rsp]
 .end:
	inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rcx, rcx
 .loop:
    cmp rcx, rsi
	jge .fail
	push rdi
	push rsi
	push rcx
	call read_char   
	pop rcx
	pop rsi
	pop rdi
	
	cmp al, space_sym
	je .check_space
	cmp al, tab_sym
	je .check_space
	cmp al, new_line_sym
	je .check_space
 .save:
	mov [rdi + rcx], al
	cmp al, null_term
	je .success
	inc rcx
	jmp .loop
	
 .check_space:
	test rcx, rcx
	jz .loop
	cmp al, new_line_sym
	jne .save

 .success:
	mov rax, rdi
	mov rdx, rcx
	jmp .end
 .fail:
	xor rax, rax
 .end:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
 .loop:
	cmp byte[rdi+rdx], '0'
	jl .end
	cmp byte[rdi+rdx], '9'
	jg .end
	push rdx
	mov rdx, 10
	mul rdx
	pop rdx
	xor r11, r11
	mov r11b, byte[rdi+rdx]
	add rax, r11
	sub rax, '0'
	inc rdx
	jmp .loop
 .end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], '-'
	je .neg
	jmp parse_uint
 .neg:
	inc rdi
	call parse_uint
	inc rdx
	neg rax
 
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
 .loop:
	cmp rax, rdx
	jge .fail
	mov r10b, byte[rdi+rax]
	mov byte[rsi+rax], r10b
	cmp byte[rsi+rax], null_term
	je .end
	inc rax
	jmp .loop
 .fail:
	xor rax, rax
 .end:
    ret

