%define label_size 8

section .text
global find_word
extern string_equals

find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
.loop:
	add r13, label_size
	mov rdi, r12
	mov rsi, r13
	call string_equals
	sub r13, label_size

	test rax, rax
	jnz .succes

	mov r13, [r13]
	test r13, r13
	jnz .loop

.fail:
	xor rax, rax
	pop r13
	pop r12
	ret
.succes:
	mov rax, r13
	pop r13
	pop r12
	ret
