%include "lib.inc"
%include "words.inc"

%define buffer_size 256
%define label_size 8

section .rodata
not_found_msg: db "Key is not found", 0
overflow_msg: db "Key is too long", 0
section .data
buffer: times buffer_size db 0

section .text
global _start
extern find_word

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	test rax, rax
	jz .overflow

	push rdx	
	mov rdi, rax
	mov rsi, next_elem
	call find_word
	pop rdx
	test rax, rax
	jz .not_found
	
	add rax, label_size
	add rax, rdx
	inc rax
	mov rdi, rax

	call print_string
	call print_newline

	xor rdi, rdi
	call exit
	
	.overflow:
		mov rdi, overflow_msg
		jmp .err_exit
		
	.not_found:
		mov rdi, not_found_msg
	.err_exit:
        	call print_err
		call print_newline
		mov rdi, 1
		call exit
	
	
	
	
